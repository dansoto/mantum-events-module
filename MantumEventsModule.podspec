Pod::Spec.new do |spec|

    spec.name         = "MantumEventsModule"
    spec.version      = "0.1.16"
    spec.summary      = "A CocoaPods library written in Swift"
  
    spec.description  = "Help mantum tu use some interfaces, that helps with the structure of iOS apps."
    
    spec.homepage     = "https://bitbucket.org/3astronautas/mantum-events-module"
    spec.license      = { :type => "MIT", :file => "LICENSE" }
    spec.author       = { "Mantum" => "juanes@tresastronautas.com" }
  
    spec.ios.deployment_target = "12.1"
    spec.swift_version = "4.2"
    spec.framework = "UIKit"
    spec.dependency 'RealmSwift' ,'~> 4.1.0'
    spec.source        = { :git => "https://bitbucket.org/3astronautas/mantum-events-module", :tag => "#{spec.version}" }
    spec.source_files  = "MantumEventsModule/**/*.{h,m,swift}"
  
  end