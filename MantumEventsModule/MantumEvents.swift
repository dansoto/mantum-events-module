//
//  MantumEvents.swift
//  MantumEventsModule
//
//  Created by juan esteban  chaparro machete on 18/11/19.
//  Copyright © 2019 Tres Astronautas. All rights reserved.
//

import Foundation

import RealmSwift

public class MantumEvents: NSObject {
    
    public var eventsService: EventsService
    
    public init(baseUrl: URL) {
        self.eventsService = EventsService(baseUrl: baseUrl)
    }
}
