//
//  DeatachableObject.swift
//  MantumServicesModule
//
//  Created by juan esteban  chaparro machete on 17/11/19.
//  Copyright © 2019 Tres Astronautas. All rights reserved.
//


import RealmSwift

protocol DetachableObject: AnyObject {
    
    func detached() -> Self
    
}

extension Object: DetachableObject {
    
    func detached() -> Self {
        let detached = type(of: self).init()
        for property in objectSchema.properties {
            guard let value = value(forKey: property.name) else { continue }
            if let detachable = value as? DetachableObject {
                detached.setValue(detachable.detached(), forKey: property.name)
            } else {
                detached.setValue(value, forKey: property.name)
            }
        }
        return detached
    }
}
