//
//  Model.swift
//  MantumComponent
//
//  Created by Jonattan Velasquez on 31/07/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

public protocol Model {
    
    func toDictionary() -> [String: Any]
    
    func toData() -> Data?
    
    func toMultipart(boundary: String) -> NSMutableData
}
