//
//  Profile.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 18/06/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

public class Profile {
    
    private let url: URL
    private let token: String
    private let model: Model?
    
    public init(url: URL, token: String = "", model: Model? = nil) {
        self.url = url
        self.token = token
        self.model = model
    }
    
    public init(url: String, token: String = "", model: Model? = nil) {
        let endpoint = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.url = URL(string: endpoint!)!
        self.token = token
        self.model = model
    }
    
    public func getUrl() -> URL {
        return self.url
    }
    
    public func getToken() -> String {
        return self.token
    }
    
    public func getModel() -> Model? {
        return self.model
    }
}
