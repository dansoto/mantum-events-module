//
//  Serializable.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 18/06/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation

public class Response {
    
    public let error: Bool
    public let message: String
    public let body: [String : Any]
    public let version: Int
    
    init(message: String, error: Bool = false, body: [String: Any] = [:], version: Int = 0) {
        self.error = error
        self.message = message
        self.body = body
        self.version = version
    }
}
