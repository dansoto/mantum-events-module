//
//  Services.swift
//  MantumComponent
//
//  Created by Jonattan Velásquez on 18/06/18.
//  Copyright © 2018 Mantum. All rights reserved.
//

import Foundation
import SystemConfiguration

open class Services {
    
    private static let ERROR_REQUEST
        = "No fue posible conectar con el servicio, por favor intentelo de nuevo"
    
    private static let ERROR_INTERNET
        = "No se tiene una conexión estable a internet"
    
    private let version: String
    
    public init() {
        self.version = "000"
    }
    
    public init(version: String) {
        self.version = version
    }
    
    public func getVersion() -> String {
        return self.version
    }
    
    private func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    public static func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    public func isErrorRequest(status: Int) -> Bool {
        return status >= 300 && status <= 599
    }
    
    private func request(request: URLRequest, profile: Profile, callback: @escaping (Response) -> ()) -> Void {
        let task = URLSession.shared.dataTask(with : request) { (data, response, error) in
            guard error == nil else {
                NSLog("Error: La petición \(profile.getUrl()) -> \(String(describing : error))")
                callback(Response(
                    message : Services.ERROR_REQUEST,
                    error : true
                ))
                return
            }
            
            guard let body = data else {
                NSLog("Error: La petición no tiene definido una data")
                callback(Response(
                    message : Services.ERROR_REQUEST,
                    error : true
                ))
                return
            }
            
            do {
                guard let json = try JSONSerialization.jsonObject(with : body, options: []) as? [String: Any] else {
                    NSLog("Error: Ocurrio un error al serializar el JSON \(body)")
                    callback(Response(
                        message : Services.ERROR_REQUEST,
                        error : true
                    ))
                    return
                }
                
                let httpResponse = response as? HTTPURLResponse
                let error = self.isErrorRequest(status: (httpResponse?.statusCode)!)
                
                var maxVersion = 0
                if let version = httpResponse?.allHeaderFields["Max-Version"] as? String {
                    maxVersion = Int(version) ?? 0
                }
                
                print("\(Date()) HTTP \(request.httpMethod ?? "No esta definido") <- \(profile.getUrl()) is \(!error ? "Ok" : "Error")")
                callback(Response(
                    message: "",
                    error: error,
                    body: json,
                    version: maxVersion
                ))
            } catch  {
                NSLog("Error: Ocurrio un error al serializar el JSON \(body)")
                callback(Response(
                    message : Services.ERROR_REQUEST,
                    error : true
                ))
            }
        }
        
        task.resume()
    }
    
    // Enviar Json
    public func post(profile: Profile, body: [String : Any], callback: @escaping (Response) -> ()) -> Void {
        if !Services.isConnectedToNetwork() {
            NSLog("No tiene conexión a internet")
            callback(Response(
                message: Services.ERROR_INTERNET,
                error: true
            ))
            return
        }
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: body, options: []) else {
            NSLog("Error: Ocurrio un error al convertir el body en JSON")
            callback(Response(
                message: Services.ERROR_REQUEST,
                error: true
            ))
            return
        }
        
        var urlRequest = URLRequest(url : profile.getUrl())
        urlRequest.httpMethod = "POST"
        urlRequest.timeoutInterval = 300
        
        if !profile.getToken().isEmpty {
            urlRequest.addValue(profile.getToken(), forHTTPHeaderField: "token")
        }
        
        urlRequest.addValue("no-cache", forHTTPHeaderField : "cache-control")
        urlRequest.addValue("application/json", forHTTPHeaderField : "content-type")
        urlRequest.addValue("application/vnd.mantum.app-v\(version)+json", forHTTPHeaderField : "accept")
        urlRequest.httpBody = httpBody
        
        print("\(Date()) HTTP POST -> \(profile.getUrl()) - version: \(version)")
        self.request(request: urlRequest, profile: profile, callback: callback)
    }
    
    // Enviar Formulario
    public func post(profile: Profile, callback: @escaping (Response) -> ()) -> Void {
        if !Services.isConnectedToNetwork() {
            NSLog("No tiene conexión a internet")
            callback(Response(
                message: Services.ERROR_INTERNET,
                error: true
            ))
            return
        }
        
        var urlRequest = URLRequest(url : profile.getUrl())
        urlRequest.httpMethod = "POST"
        urlRequest.timeoutInterval = 300
        
        if !profile.getToken().isEmpty {
            urlRequest.addValue(profile.getToken(), forHTTPHeaderField: "token")
        }
        
        let boundary = generateBoundaryString()
        urlRequest.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField : "content-type")
        urlRequest.addValue("no-cache", forHTTPHeaderField : "cache-control")
        urlRequest.addValue("application/json", forHTTPHeaderField : "content-type")
        urlRequest.addValue("application/vnd.mantum.app-v\(version)+json", forHTTPHeaderField : "accept")
        if let model = profile.getModel() {
            urlRequest.httpBody = model.toMultipart(boundary: boundary) as Data
        }
        
        print("\(Date()) HTTP POST -> \(profile.getUrl())")
        self.request(request: urlRequest, profile: profile, callback: callback)
    }
    
    public func get(profile: Profile, callback: @escaping (Response) -> ()) -> Void {
        if !Services.isConnectedToNetwork() {
            NSLog("No tiene conexión a internet")
            callback(Response(
                message: Services.ERROR_INTERNET,
                error: true
            ))
            return
        }
        
        var urlRequest = URLRequest(url : profile.getUrl())
        urlRequest.httpMethod = "GET"
        urlRequest.timeoutInterval = 300
        
        if !profile.getToken().isEmpty {
            urlRequest.addValue(profile.getToken(), forHTTPHeaderField: "token")
        }
        
        urlRequest.addValue("no-cache", forHTTPHeaderField : "cache-control")
        urlRequest.addValue("application/json", forHTTPHeaderField : "content-type")
        urlRequest.addValue("application/vnd.mantum.app-v\(version)+json", forHTTPHeaderField : "accept")
        
        print("\(Date()) HTTP GET -> \(profile.getUrl())")
        self.request(request: urlRequest, profile: profile, callback: callback)
    }
    
    public static func prepare(boundary: String, file: (key: String, value: Data)) -> NSMutableData {
        let mimeType = "image/jpg"
        let boundaryPrefix = "--\(boundary)\r\n"
        
        let body = NSMutableData()
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(file.key).jpg\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(file.value)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        return body
    }
    
    public func pagination<T>(page: Int, values : [T]) -> [T] {
        let total = values.count
        if total == 0 {
            return []
        }
        
        let min = (page == 1 ? 0 : page - 1) * 10
        if min >= total {
            return []
        }
        
        var max = min + 10
        max = max > total ? total : max
        
        NSLog("Pagination \(page) [\(min), \(max)] to \(total)")
        return Array(values[min..<max])
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
