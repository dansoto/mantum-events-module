//
//  Service.swift
//  MantumServices
//
//  Created by juan esteban  chaparro machete on 14/11/19.
//  Copyright © 2019 Tres Astronautas. All rights reserved.
//

import Foundation
import RealmSwift

public class Evento: Object {
    
    @objc public dynamic var id: Int64 = 0
    @objc public dynamic var nombre: String = ""
    @objc public dynamic var descripcion: String = ""
    @objc public dynamic var horario: String = ""
    @objc public dynamic var piso: String = ""
    @objc public dynamic var zona: String = ""
    @objc public dynamic var fechainicio: String = ""
    @objc public dynamic var fechafin: String = ""
    @objc public dynamic var logo: String = ""
    @objc public dynamic var ubicacion: String = ""
    @objc public dynamic var color: String = ""
    @objc public dynamic var orden: Int = 0
    
    override public static func primaryKey() -> String? {
        return "id"
    }
    
    public func toDictionary() -> [String : Any] {
        return [
            "id" : self.id,
            "nombre" : self.nombre,
            "descripcion": self.descripcion,
            "horario" : self.horario,
            "piso" : self.piso,
            "zona" : self.zona,
            "logo" : self.logo,
            "ubicacion" : self.ubicacion,
            "fechainicio" : self.fechainicio,
            "fechafin" : self.fechafin
        ]
    }
    
    public func toJson() -> String? {
        let data = try! JSONSerialization.data(withJSONObject: toDictionary(), options: [])
        return String(data: data, encoding: .utf8) ?? nil
    }
    
    static func fromJson(json: [String: Any]) -> Evento {
        let evento = Evento()
        if let id = json["id"] as? Int64 {
            evento.id = id
        }
        if let ub = json["ubicacion"] as? Int64 {
            evento.ubicacion = ub.description
        }
        if let nombre = json["nombre"] as? String {
            evento.nombre = nombre
        }
        if let descripcion = json["descripcion"] as? String {
            evento.descripcion = descripcion
        }
        if let fechaInicio = json["fechainicio"] as? String {
            evento.fechainicio = fechaInicio
        }
        if let fechaFin = json["fechafin"] as? String {
            evento.fechafin = fechaFin
        }
        if let horario = json["horario"] as? String {
            evento.horario = horario
        }
        if let piso = json["piso"] as? String {
            evento.piso = piso
        }
        if let zona = json["zona"] as? String {
            evento.zona = zona
        }
        if let logo = json["logo"] as? String {
            evento.logo = logo
        }
        return evento;
    }
    
}




