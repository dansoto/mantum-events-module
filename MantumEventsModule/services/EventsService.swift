//
//  EventsService.swift
//  MantumServices
//
//  Created by juan esteban  chaparro machete on 14/11/19.
//  Copyright © 2019 Tres Astronautas. All rights reserved.
//

import Foundation
import RealmSwift

public class EventsService: Services {
    
    private let url: URL

    
    public init(baseUrl: URL) {
        self.url = baseUrl
        super.init()
        
    }
    
    public func eventos() -> [Evento] {
        let realm = try! Realm()
        return realm.objects(Evento.self)
               .sorted(byKeyPath: "orden", ascending: true)
               .map { (event) -> Evento in event.detached() }
       }
    
    public func eventos(callback: @escaping ([Evento]) -> ()) -> Void {
       
        
        let profile = Profile(url: self.url)
        self.get(profile: profile) { (response) in
            
            let realm = try! Realm()
            if let body = response.body["oData"] as? [String: Any] {
                
                if let elementos = body["aEventos"] as? [[String: Any]] {
                    var orden: Int = 0
                    var idEventos: [Int64] = []
                    var eventos: [Evento] = []
                    
                    // Agrega las campañas a la base de datos
                    elementos.forEach({ (json) in
                        let evento = Evento.fromJson(json: json)
                       
                        idEventos.append(evento.id)
                        eventos.append(evento)
                        orden = orden + 1
                        evento.orden = orden
                        try! realm.write {
                            realm.create(Evento.self, value: evento, update: .all)
                            
                        }
                    })
                    
                    // Elimina los elementos que el REST no retorna
                    let query = idEventos.compactMap({String($0)}).joined(separator: ",")
                    let resultado = realm.objects(Evento.self)
                        .filter("NOT id IN {\(query)}")
                    
                    if !resultado.isEmpty {
                        try! realm.write {
                            realm.delete(resultado)
                            NSLog("Eliminar \(resultado.count) eventos")
                        }
                    }
                    
                    callback(eventos)
                }
            }
        }
        
        // Obtiene los eventos guardados en la base de datos
        NSLog("Obteniendo eventos de la base de datos")
        callback(self.eventos())
    }
    
}
